﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../Actor Framework PPL/Actor Framework.lvlibp">
			<Item Name="Messages" Type="Folder">
				<Item Name="Message.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message/Message.lvclass"/>
				<Item Name="Stop Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Stop Msg/Stop Msg.lvclass"/>
				<Item Name="Last Ack.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/LastAck/Last Ack.lvclass"/>
				<Item Name="Launch Nested Actor Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Launch Nested Actor Msg/Launch Nested Actor Msg.lvclass"/>
			</Item>
			<Item Name="Time-Delayed Send Message" Type="Folder">
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
			</Item>
			<Item Name="Actor.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Actor/Actor.lvclass"/>
			<Item Name="Message Priority Queue.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Priority Queue/Message Priority Queue.lvclass"/>
			<Item Name="Message Enqueuer.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Enqueuer/Message Enqueuer.lvclass"/>
			<Item Name="Message Dequeuer.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Dequeuer/Message Dequeuer.lvclass"/>
			<Item Name="Message Queue.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Message Queue/Message Queue.lvclass"/>
			<Item Name="Init Actor Queues FOR TESTING ONLY.vi" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Actor/Init Actor Queues FOR TESTING ONLY.vi"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/resource/AFDebug/AF Debug.lvlib"/>
			<Item Name="Batch Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
			<Item Name="Reply Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Reply Msg/Reply Msg.lvclass"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
			<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../Actor Framework PPL/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
		</Item>
		<Item Name="Sample PPL Actor.lvlib" Type="Library" URL="../Sample PPL Actor/Sample PPL Actor.lvlib"/>
		<Item Name="Specific Cases PPL.lvlib" Type="Library" URL="../Specific Cases PPL/Specific Cases PPL.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
