**Bug Report**
Template Version 1.0.1.1

---

**Summary**


(Summarize the bug encountered concisely)

---

**What is the current bug behavior?**

(What actually happens)

---

**What is the expected correct behavior?**

(What you should see instead)

---

**Found in**  


( If source code add sha hash of commit)
( If exe add version number)

---

**Steps to reproduce**

(How one can reproduce the issue - this is very important)

---

**Relevant logs and/or screenshots**

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

---

**Possible causes**

(If you can, identify the vi or module that think might be resposible)

---

**Unit Testing** (If customer don't touch this section.  Otherwise add an x as appropriate.)

* [ ]  I have created a unit test to catch the condition and verified that it caught the condition. **Required before fixing**
* [ ]  I have run the updated unit tests and they now pass. **Required before closing**

/label ~"Bug" ~"To Do"
/estimate (insert estimate here ie. 1d, 1w, 1m etc.)